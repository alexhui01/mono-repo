/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export type AddedSharedMember = ContractEventLog<{
  roleId: string;
  newMember: string;
  manager: string;
  0: string;
  1: string;
  2: string;
}>;
export type NewFinalFee = ContractEventLog<{
  newFinalFee: [string];
  0: [string];
}>;
export type NewFixedOracleFeePerSecondPerPfc = ContractEventLog<{
  newOracleFee: [string];
  0: [string];
}>;
export type NewWeeklyDelayFeePerSecondPerPfc = ContractEventLog<{
  newWeeklyDelayFeePerSecondPerPfc: [string];
  0: [string];
}>;
export type RemovedSharedMember = ContractEventLog<{
  roleId: string;
  oldMember: string;
  manager: string;
  0: string;
  1: string;
  2: string;
}>;
export type ResetExclusiveMember = ContractEventLog<{
  roleId: string;
  newMember: string;
  manager: string;
  0: string;
  1: string;
  2: string;
}>;

export interface Store extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): Store;
  clone(): Store;
  methods: {
    SECONDS_PER_WEEK(): NonPayableTransactionObject<string>;

    addMember(
      roleId: number | string,
      newMember: string
    ): NonPayableTransactionObject<void>;

    finalFees(arg0: string): NonPayableTransactionObject<string>;

    fixedOracleFeePerSecondPerPfc(): NonPayableTransactionObject<string>;

    getCurrentTime(): NonPayableTransactionObject<string>;

    getMember(roleId: number | string): NonPayableTransactionObject<string>;

    holdsRole(
      roleId: number | string,
      memberToCheck: string
    ): NonPayableTransactionObject<boolean>;

    removeMember(
      roleId: number | string,
      memberToRemove: string
    ): NonPayableTransactionObject<void>;

    renounceMembership(
      roleId: number | string
    ): NonPayableTransactionObject<void>;

    resetMember(
      roleId: number | string,
      newMember: string
    ): NonPayableTransactionObject<void>;

    setCurrentTime(time: number | string): NonPayableTransactionObject<void>;

    timerAddress(): NonPayableTransactionObject<string>;

    weeklyDelayFeePerSecondPerPfc(): NonPayableTransactionObject<string>;

    withdraw(amount: number | string): NonPayableTransactionObject<void>;

    withdrawErc20(
      erc20Address: string,
      amount: number | string
    ): NonPayableTransactionObject<void>;

    payOracleFees(): PayableTransactionObject<void>;

    payOracleFeesErc20(
      erc20Address: string,
      amount: [number | string]
    ): NonPayableTransactionObject<void>;

    computeRegularFee(
      startTime: number | string,
      endTime: number | string,
      pfc: [number | string]
    ): NonPayableTransactionObject<{
      regularFee: [string];
      latePenalty: [string];
      0: [string];
      1: [string];
    }>;

    computeFinalFee(currency: string): NonPayableTransactionObject<[string]>;

    setFixedOracleFeePerSecondPerPfc(
      newFixedOracleFeePerSecondPerPfc: [number | string]
    ): NonPayableTransactionObject<void>;

    setWeeklyDelayFeePerSecondPerPfc(
      newWeeklyDelayFeePerSecondPerPfc: [number | string]
    ): NonPayableTransactionObject<void>;

    setFinalFee(
      currency: string,
      newFinalFee: [number | string]
    ): NonPayableTransactionObject<void>;
  };
  events: {
    AddedSharedMember(cb?: Callback<AddedSharedMember>): EventEmitter;
    AddedSharedMember(
      options?: EventOptions,
      cb?: Callback<AddedSharedMember>
    ): EventEmitter;

    NewFinalFee(cb?: Callback<NewFinalFee>): EventEmitter;
    NewFinalFee(
      options?: EventOptions,
      cb?: Callback<NewFinalFee>
    ): EventEmitter;

    NewFixedOracleFeePerSecondPerPfc(
      cb?: Callback<NewFixedOracleFeePerSecondPerPfc>
    ): EventEmitter;
    NewFixedOracleFeePerSecondPerPfc(
      options?: EventOptions,
      cb?: Callback<NewFixedOracleFeePerSecondPerPfc>
    ): EventEmitter;

    NewWeeklyDelayFeePerSecondPerPfc(
      cb?: Callback<NewWeeklyDelayFeePerSecondPerPfc>
    ): EventEmitter;
    NewWeeklyDelayFeePerSecondPerPfc(
      options?: EventOptions,
      cb?: Callback<NewWeeklyDelayFeePerSecondPerPfc>
    ): EventEmitter;

    RemovedSharedMember(cb?: Callback<RemovedSharedMember>): EventEmitter;
    RemovedSharedMember(
      options?: EventOptions,
      cb?: Callback<RemovedSharedMember>
    ): EventEmitter;

    ResetExclusiveMember(cb?: Callback<ResetExclusiveMember>): EventEmitter;
    ResetExclusiveMember(
      options?: EventOptions,
      cb?: Callback<ResetExclusiveMember>
    ): EventEmitter;

    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };

  once(event: "AddedSharedMember", cb: Callback<AddedSharedMember>): void;
  once(
    event: "AddedSharedMember",
    options: EventOptions,
    cb: Callback<AddedSharedMember>
  ): void;

  once(event: "NewFinalFee", cb: Callback<NewFinalFee>): void;
  once(
    event: "NewFinalFee",
    options: EventOptions,
    cb: Callback<NewFinalFee>
  ): void;

  once(
    event: "NewFixedOracleFeePerSecondPerPfc",
    cb: Callback<NewFixedOracleFeePerSecondPerPfc>
  ): void;
  once(
    event: "NewFixedOracleFeePerSecondPerPfc",
    options: EventOptions,
    cb: Callback<NewFixedOracleFeePerSecondPerPfc>
  ): void;

  once(
    event: "NewWeeklyDelayFeePerSecondPerPfc",
    cb: Callback<NewWeeklyDelayFeePerSecondPerPfc>
  ): void;
  once(
    event: "NewWeeklyDelayFeePerSecondPerPfc",
    options: EventOptions,
    cb: Callback<NewWeeklyDelayFeePerSecondPerPfc>
  ): void;

  once(event: "RemovedSharedMember", cb: Callback<RemovedSharedMember>): void;
  once(
    event: "RemovedSharedMember",
    options: EventOptions,
    cb: Callback<RemovedSharedMember>
  ): void;

  once(event: "ResetExclusiveMember", cb: Callback<ResetExclusiveMember>): void;
  once(
    event: "ResetExclusiveMember",
    options: EventOptions,
    cb: Callback<ResetExclusiveMember>
  ): void;
}
