/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export type OwnershipTransferred = ContractEventLog<{
  previousOwner: string;
  newOwner: string;
  0: string;
  1: string;
}>;
export type SupportedIdentifierAdded = ContractEventLog<{
  identifier: string;
  0: string;
}>;
export type SupportedIdentifierRemoved = ContractEventLog<{
  identifier: string;
  0: string;
}>;

export interface IdentifierWhitelist extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): IdentifierWhitelist;
  clone(): IdentifierWhitelist;
  methods: {
    owner(): NonPayableTransactionObject<string>;

    renounceOwnership(): NonPayableTransactionObject<void>;

    transferOwnership(newOwner: string): NonPayableTransactionObject<void>;

    addSupportedIdentifier(
      identifier: string | number[]
    ): NonPayableTransactionObject<void>;

    removeSupportedIdentifier(
      identifier: string | number[]
    ): NonPayableTransactionObject<void>;

    isIdentifierSupported(
      identifier: string | number[]
    ): NonPayableTransactionObject<boolean>;
  };
  events: {
    OwnershipTransferred(cb?: Callback<OwnershipTransferred>): EventEmitter;
    OwnershipTransferred(
      options?: EventOptions,
      cb?: Callback<OwnershipTransferred>
    ): EventEmitter;

    SupportedIdentifierAdded(
      cb?: Callback<SupportedIdentifierAdded>
    ): EventEmitter;
    SupportedIdentifierAdded(
      options?: EventOptions,
      cb?: Callback<SupportedIdentifierAdded>
    ): EventEmitter;

    SupportedIdentifierRemoved(
      cb?: Callback<SupportedIdentifierRemoved>
    ): EventEmitter;
    SupportedIdentifierRemoved(
      options?: EventOptions,
      cb?: Callback<SupportedIdentifierRemoved>
    ): EventEmitter;

    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };

  once(event: "OwnershipTransferred", cb: Callback<OwnershipTransferred>): void;
  once(
    event: "OwnershipTransferred",
    options: EventOptions,
    cb: Callback<OwnershipTransferred>
  ): void;

  once(
    event: "SupportedIdentifierAdded",
    cb: Callback<SupportedIdentifierAdded>
  ): void;
  once(
    event: "SupportedIdentifierAdded",
    options: EventOptions,
    cb: Callback<SupportedIdentifierAdded>
  ): void;

  once(
    event: "SupportedIdentifierRemoved",
    cb: Callback<SupportedIdentifierRemoved>
  ): void;
  once(
    event: "SupportedIdentifierRemoved",
    options: EventOptions,
    cb: Callback<SupportedIdentifierRemoved>
  ): void;
}
