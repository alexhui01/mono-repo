/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export interface ISynthereumPoolOnChainPriceFeed extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): ISynthereumPoolOnChainPriceFeed;
  clone(): ISynthereumPoolOnChainPriceFeed;
  methods: {
    collateralToken(): NonPayableTransactionObject<string>;

    /**
     * Called by a source Pool's `exchange` function to mint destination tokensThis functon can be called only by a pool registred in the PoolRegister contract
     * @param collateralAmount The amount of collateral to use from the source Pool
     * @param derivative The derivative of the destination pool to use for mint
     * @param numTokens The number of new tokens to mint
     * @param srcDerivative Derivative used by the source pool
     */
    exchangeMint(
      srcDerivative: string,
      derivative: string,
      collateralAmount: number | string,
      numTokens: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Returns price identifier of the pool
     */
    getPriceFeedIdentifier(): NonPayableTransactionObject<string>;

    /**
     * Check if a derivative is in the whitelist of this pool
     * @param derivative Perpetual derivative
     */
    isDerivativeAdmitted(
      derivative: string
    ): NonPayableTransactionObject<boolean>;

    synthereumFinder(): NonPayableTransactionObject<string>;

    syntheticToken(): NonPayableTransactionObject<string>;

    syntheticTokenSymbol(): NonPayableTransactionObject<string>;

    version(): NonPayableTransactionObject<string>;

    /**
     * Add a derivate to be controlled by this pool
     * @param derivative A perpetual derivative
     */
    addDerivative(derivative: string): NonPayableTransactionObject<void>;

    /**
     * Remove a derivative controlled by this pool
     * @param derivative A perpetual derivative
     */
    removeDerivative(derivative: string): NonPayableTransactionObject<void>;

    mint(
      mintParams: [
        string,
        number | string,
        number | string,
        number | string,
        number | string
      ]
    ): NonPayableTransactionObject<{
      syntheticTokensMinted: string;
      feePaid: string;
      0: string;
      1: string;
    }>;

    redeem(
      redeemParams: [
        string,
        number | string,
        number | string,
        number | string,
        number | string
      ]
    ): NonPayableTransactionObject<{
      collateralRedeemed: string;
      feePaid: string;
      0: string;
      1: string;
    }>;

    exchange(
      exchangeParams: [
        string,
        string,
        string,
        number | string,
        number | string,
        number | string,
        number | string
      ]
    ): NonPayableTransactionObject<{
      destNumTokensMinted: string;
      feePaid: string;
      0: string;
      1: string;
    }>;

    /**
     * Liquidity provider withdraw margin from the pool
     * @param collateralAmount The amount of margin to withdraw
     */
    withdrawFromPool(
      collateralAmount: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Move collateral from Pool to its derivative in order to increase GCR
     * @param collateralAmount The amount of collateral to move into derivative
     * @param derivative Derivative on which to deposit collateral
     */
    depositIntoDerivative(
      derivative: string,
      collateralAmount: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Start a slow withdrawal requestCollateral can be withdrawn once the liveness period has elapsed
     * @param collateralAmount The amount of excess collateral to withdraw
     * @param derivative Derivative from which collateral withdrawal is requested
     */
    slowWithdrawRequest(
      derivative: string,
      collateralAmount: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Withdraw collateral after a withdraw request has passed it's liveness period
     * @param derivative Derivative from which collateral withdrawal is requested
     */
    slowWithdrawPassedRequest(
      derivative: string
    ): NonPayableTransactionObject<string>;

    /**
     * Withdraw collateral immediately if the remaining collateral is above GCR
     * @param collateralAmount The amount of excess collateral to withdraw
     * @param derivative Derivative from which fast withdrawal is requested
     */
    fastWithdraw(
      derivative: string,
      collateralAmount: number | string
    ): NonPayableTransactionObject<string>;

    /**
     * Activate emergency shutdown on a derivative in order to liquidate the token holders in case of emergency
     * @param derivative Derivative on which the emergency shutdown is called
     */
    emergencyShutdown(derivative: string): NonPayableTransactionObject<void>;

    /**
     * Redeem tokens after contract emergency shutdown
     * @param derivative Derivative for which settlement is requested
     */
    settleEmergencyShutdown(
      derivative: string
    ): NonPayableTransactionObject<string>;

    setFee(
      _fee: [[number | string], string[], (number | string)[]]
    ): NonPayableTransactionObject<void>;

    /**
     * Update the fee percentage
     * @param _feePercentage The new fee percentage
     */
    setFeePercentage(
      _feePercentage: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Update the addresses of recipients for generated fees and proportions of fees each address will receive
     * @param _feeProportions An array of the proportions of fees generated each recipient will receive
     * @param _feeRecipients An array of the addresses of recipients that will receive generated fees
     */
    setFeeRecipients(
      _feeRecipients: string[],
      _feeProportions: (number | string)[]
    ): NonPayableTransactionObject<void>;

    /**
     * Reset the starting collateral ratio - for example when you add a new derivative without collateral
     * @param startingCollateralRatio Initial ratio between collateral amount and synth tokens
     */
    setStartingCollateralization(
      startingCollateralRatio: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Add a role into derivative to another contract
     * @param addressToAdd address of EOA or smart contract to add with a role in derivative
     * @param derivative Derivative in which a role is added
     * @param derivativeRole Role to add
     */
    addRoleInDerivative(
      derivative: string,
      derivativeRole: number | string,
      addressToAdd: string
    ): NonPayableTransactionObject<void>;

    /**
     * This pool renounce a role in the derivative
     * @param derivative Derivative in which a role is renounced
     * @param derivativeRole Role to renounce
     */
    renounceRoleInDerivative(
      derivative: string,
      derivativeRole: number | string
    ): NonPayableTransactionObject<void>;

    /**
     * Add a role into synthetic token to another contract
     * @param addressToAdd address of EOA or smart contract to add with a role in derivative
     * @param derivative Derivative in which a role is added
     * @param synthTokenRole Role to add
     */
    addRoleInSynthToken(
      derivative: string,
      synthTokenRole: number | string,
      addressToAdd: string
    ): NonPayableTransactionObject<void>;

    /**
     * Set the possibility to accept only EOA meta-tx
     * @param isContractAllowed Flag that represent options to receive tx by a contract or only EOA
     */
    setIsContractAllowed(
      isContractAllowed: boolean
    ): NonPayableTransactionObject<void>;

    /**
     * Get all the derivatives associated to this pool
     */
    getAllDerivatives(): NonPayableTransactionObject<string[]>;

    /**
     * Get the starting collateral ratio of the pool
     */
    getStartingCollateralization(): NonPayableTransactionObject<string>;

    /**
     * Returns if pool can accept only EOA meta-tx or also contract meta-tx
     */
    isContractAllowed(): NonPayableTransactionObject<boolean>;

    /**
     * Returns infos about fee set
     */
    getFeeInfo(): NonPayableTransactionObject<[[string], string[], string[]]>;

    /**
     * Calculate the fees a user will have to pay to mint tokens with their collateral
     * @param collateralAmount Amount of collateral on which fees are calculated
     */
    calculateFee(
      collateralAmount: number | string
    ): NonPayableTransactionObject<string>;
  };
  events: {
    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };
}
