/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export type DisputePrice = ContractEventLog<{
  requester: string;
  proposer: string;
  disputer: string;
  identifier: string;
  timestamp: string;
  ancillaryData: string;
  0: string;
  1: string;
  2: string;
  3: string;
  4: string;
  5: string;
}>;
export type ProposePrice = ContractEventLog<{
  requester: string;
  proposer: string;
  identifier: string;
  timestamp: string;
  ancillaryData: string;
  proposedPrice: string;
  0: string;
  1: string;
  2: string;
  3: string;
  4: string;
  5: string;
}>;
export type RequestPrice = ContractEventLog<{
  requester: string;
  identifier: string;
  timestamp: string;
  ancillaryData: string;
  currency: string;
  reward: string;
  finalFee: string;
  0: string;
  1: string;
  2: string;
  3: string;
  4: string;
  5: string;
  6: string;
}>;
export type Settle = ContractEventLog<{
  requester: string;
  proposer: string;
  disputer: string;
  identifier: string;
  timestamp: string;
  ancillaryData: string;
  price: string;
  payout: string;
  0: string;
  1: string;
  2: string;
  3: string;
  4: string;
  5: string;
  6: string;
  7: string;
}>;

export interface OptimisticOracle extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): OptimisticOracle;
  clone(): OptimisticOracle;
  methods: {
    ancillaryBytesLimit(): NonPayableTransactionObject<string>;

    defaultLiveness(): NonPayableTransactionObject<string>;

    finder(): NonPayableTransactionObject<string>;

    getCurrentTime(): NonPayableTransactionObject<string>;

    requests(
      arg0: string | number[]
    ): NonPayableTransactionObject<{
      proposer: string;
      disputer: string;
      currency: string;
      settled: boolean;
      refundOnDispute: boolean;
      proposedPrice: string;
      resolvedPrice: string;
      expirationTime: string;
      reward: string;
      finalFee: string;
      bond: string;
      customLiveness: string;
      0: string;
      1: string;
      2: string;
      3: boolean;
      4: boolean;
      5: string;
      6: string;
      7: string;
      8: string;
      9: string;
      10: string;
      11: string;
    }>;

    setCurrentTime(time: number | string): NonPayableTransactionObject<void>;

    timerAddress(): NonPayableTransactionObject<string>;

    requestPrice(
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[],
      currency: string,
      reward: number | string
    ): NonPayableTransactionObject<string>;

    setBond(
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[],
      bond: number | string
    ): NonPayableTransactionObject<string>;

    setRefundOnDispute(
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<void>;

    setCustomLiveness(
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[],
      customLiveness: number | string
    ): NonPayableTransactionObject<void>;

    proposePriceFor(
      proposer: string,
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[],
      proposedPrice: number | string
    ): NonPayableTransactionObject<string>;

    proposePrice(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[],
      proposedPrice: number | string
    ): NonPayableTransactionObject<string>;

    disputePriceFor(
      disputer: string,
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<string>;

    disputePrice(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<string>;

    settleAndGetPrice(
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<string>;

    settle(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<string>;

    getRequest(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<
      [
        string,
        string,
        string,
        boolean,
        boolean,
        string,
        string,
        string,
        string,
        string,
        string,
        string
      ]
    >;

    getState(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<string>;

    hasPrice(
      requester: string,
      identifier: string | number[],
      timestamp: number | string,
      ancillaryData: string | number[]
    ): NonPayableTransactionObject<boolean>;

    stampAncillaryData(
      ancillaryData: string | number[],
      requester: string
    ): NonPayableTransactionObject<string>;
  };
  events: {
    DisputePrice(cb?: Callback<DisputePrice>): EventEmitter;
    DisputePrice(
      options?: EventOptions,
      cb?: Callback<DisputePrice>
    ): EventEmitter;

    ProposePrice(cb?: Callback<ProposePrice>): EventEmitter;
    ProposePrice(
      options?: EventOptions,
      cb?: Callback<ProposePrice>
    ): EventEmitter;

    RequestPrice(cb?: Callback<RequestPrice>): EventEmitter;
    RequestPrice(
      options?: EventOptions,
      cb?: Callback<RequestPrice>
    ): EventEmitter;

    Settle(cb?: Callback<Settle>): EventEmitter;
    Settle(options?: EventOptions, cb?: Callback<Settle>): EventEmitter;

    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };

  once(event: "DisputePrice", cb: Callback<DisputePrice>): void;
  once(
    event: "DisputePrice",
    options: EventOptions,
    cb: Callback<DisputePrice>
  ): void;

  once(event: "ProposePrice", cb: Callback<ProposePrice>): void;
  once(
    event: "ProposePrice",
    options: EventOptions,
    cb: Callback<ProposePrice>
  ): void;

  once(event: "RequestPrice", cb: Callback<RequestPrice>): void;
  once(
    event: "RequestPrice",
    options: EventOptions,
    cb: Callback<RequestPrice>
  ): void;

  once(event: "Settle", cb: Callback<Settle>): void;
  once(event: "Settle", options: EventOptions, cb: Callback<Settle>): void;
}
