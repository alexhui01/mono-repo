/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export interface ISynthereumDeployer extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): ISynthereumDeployer;
  clone(): ISynthereumDeployer;
  methods: {
    deployPoolAndDerivative(
      derivativeVersion: number | string,
      poolVersion: number | string,
      derivativeParamsData: string | number[],
      poolParamsData: string | number[]
    ): NonPayableTransactionObject<{
      derivative: string;
      pool: string;
      0: string;
      1: string;
    }>;

    deployOnlyPool(
      poolVersion: number | string,
      poolParamsData: string | number[],
      derivative: string
    ): NonPayableTransactionObject<string>;

    deployOnlyDerivative(
      derivativeVersion: number | string,
      derivativeParamsData: string | number[],
      pool: string
    ): NonPayableTransactionObject<string>;
  };
  events: {
    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };
}
