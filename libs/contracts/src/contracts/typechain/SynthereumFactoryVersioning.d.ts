/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { ContractOptions } from "web3-eth-contract";
import { EventLog } from "web3-core";
import { EventEmitter } from "events";
import {
  Callback,
  PayableTransactionObject,
  NonPayableTransactionObject,
  BlockType,
  ContractEventLog,
  BaseContract,
} from "./types";

interface EventOptions {
  filter?: object;
  fromBlock?: BlockType;
  topics?: string[];
}

export type AddDerivativeFactory = ContractEventLog<{
  version: string;
  derivativeFactory: string;
  0: string;
  1: string;
}>;
export type AddPoolFactory = ContractEventLog<{
  version: string;
  poolFactory: string;
  0: string;
  1: string;
}>;
export type RemoveDerivativeFactory = ContractEventLog<{
  version: string;
  0: string;
}>;
export type RemovePoolFactory = ContractEventLog<{
  version: string;
  0: string;
}>;
export type RoleAdminChanged = ContractEventLog<{
  role: string;
  previousAdminRole: string;
  newAdminRole: string;
  0: string;
  1: string;
  2: string;
}>;
export type RoleGranted = ContractEventLog<{
  role: string;
  account: string;
  sender: string;
  0: string;
  1: string;
  2: string;
}>;
export type RoleRevoked = ContractEventLog<{
  role: string;
  account: string;
  sender: string;
  0: string;
  1: string;
  2: string;
}>;

export interface SynthereumFactoryVersioning extends BaseContract {
  constructor(
    jsonInterface: any[],
    address?: string,
    options?: ContractOptions
  ): SynthereumFactoryVersioning;
  clone(): SynthereumFactoryVersioning;
  methods: {
    DEFAULT_ADMIN_ROLE(): NonPayableTransactionObject<string>;

    MAINTAINER_ROLE(): NonPayableTransactionObject<string>;

    getRoleAdmin(role: string | number[]): NonPayableTransactionObject<string>;

    getRoleMember(
      role: string | number[],
      index: number | string
    ): NonPayableTransactionObject<string>;

    getRoleMemberCount(
      role: string | number[]
    ): NonPayableTransactionObject<string>;

    grantRole(
      role: string | number[],
      account: string
    ): NonPayableTransactionObject<void>;

    hasRole(
      role: string | number[],
      account: string
    ): NonPayableTransactionObject<boolean>;

    renounceRole(
      role: string | number[],
      account: string
    ): NonPayableTransactionObject<void>;

    revokeRole(
      role: string | number[],
      account: string
    ): NonPayableTransactionObject<void>;

    setPoolFactory(
      version: number | string,
      poolFactory: string
    ): NonPayableTransactionObject<void>;

    removePoolFactory(
      version: number | string
    ): NonPayableTransactionObject<void>;

    setDerivativeFactory(
      version: number | string,
      derivativeFactory: string
    ): NonPayableTransactionObject<void>;

    removeDerivativeFactory(
      version: number | string
    ): NonPayableTransactionObject<void>;

    getPoolFactoryVersion(
      version: number | string
    ): NonPayableTransactionObject<string>;

    numberOfVerisonsOfPoolFactory(): NonPayableTransactionObject<string>;

    getDerivativeFactoryVersion(
      version: number | string
    ): NonPayableTransactionObject<string>;

    numberOfVerisonsOfDerivativeFactory(): NonPayableTransactionObject<string>;
  };
  events: {
    AddDerivativeFactory(cb?: Callback<AddDerivativeFactory>): EventEmitter;
    AddDerivativeFactory(
      options?: EventOptions,
      cb?: Callback<AddDerivativeFactory>
    ): EventEmitter;

    AddPoolFactory(cb?: Callback<AddPoolFactory>): EventEmitter;
    AddPoolFactory(
      options?: EventOptions,
      cb?: Callback<AddPoolFactory>
    ): EventEmitter;

    RemoveDerivativeFactory(
      cb?: Callback<RemoveDerivativeFactory>
    ): EventEmitter;
    RemoveDerivativeFactory(
      options?: EventOptions,
      cb?: Callback<RemoveDerivativeFactory>
    ): EventEmitter;

    RemovePoolFactory(cb?: Callback<RemovePoolFactory>): EventEmitter;
    RemovePoolFactory(
      options?: EventOptions,
      cb?: Callback<RemovePoolFactory>
    ): EventEmitter;

    RoleAdminChanged(cb?: Callback<RoleAdminChanged>): EventEmitter;
    RoleAdminChanged(
      options?: EventOptions,
      cb?: Callback<RoleAdminChanged>
    ): EventEmitter;

    RoleGranted(cb?: Callback<RoleGranted>): EventEmitter;
    RoleGranted(
      options?: EventOptions,
      cb?: Callback<RoleGranted>
    ): EventEmitter;

    RoleRevoked(cb?: Callback<RoleRevoked>): EventEmitter;
    RoleRevoked(
      options?: EventOptions,
      cb?: Callback<RoleRevoked>
    ): EventEmitter;

    allEvents(options?: EventOptions, cb?: Callback<EventLog>): EventEmitter;
  };

  once(event: "AddDerivativeFactory", cb: Callback<AddDerivativeFactory>): void;
  once(
    event: "AddDerivativeFactory",
    options: EventOptions,
    cb: Callback<AddDerivativeFactory>
  ): void;

  once(event: "AddPoolFactory", cb: Callback<AddPoolFactory>): void;
  once(
    event: "AddPoolFactory",
    options: EventOptions,
    cb: Callback<AddPoolFactory>
  ): void;

  once(
    event: "RemoveDerivativeFactory",
    cb: Callback<RemoveDerivativeFactory>
  ): void;
  once(
    event: "RemoveDerivativeFactory",
    options: EventOptions,
    cb: Callback<RemoveDerivativeFactory>
  ): void;

  once(event: "RemovePoolFactory", cb: Callback<RemovePoolFactory>): void;
  once(
    event: "RemovePoolFactory",
    options: EventOptions,
    cb: Callback<RemovePoolFactory>
  ): void;

  once(event: "RoleAdminChanged", cb: Callback<RoleAdminChanged>): void;
  once(
    event: "RoleAdminChanged",
    options: EventOptions,
    cb: Callback<RoleAdminChanged>
  ): void;

  once(event: "RoleGranted", cb: Callback<RoleGranted>): void;
  once(
    event: "RoleGranted",
    options: EventOptions,
    cb: Callback<RoleGranted>
  ): void;

  once(event: "RoleRevoked", cb: Callback<RoleRevoked>): void;
  once(
    event: "RoleRevoked",
    options: EventOptions,
    cb: Callback<RoleRevoked>
  ): void;
}
