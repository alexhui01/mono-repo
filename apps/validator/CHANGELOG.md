# 1.0.0 (2020-12-22)


### Features

* **root:** :fire: Add semantic-versioning ([d7e9b4d](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/d7e9b4df5842e8531b4f85d0c5c1e24cd01d3e81))
* **root:** :hammer: Change the registry ([e712521](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/e712521eed64bd80bb81adbf04b23a76e3e7f08d))
* **root:** :tada: Move the devDependencies to root package.json ([5d4067a](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/5d4067a8c8e61107fb8814d3dce6feab02d0682d))
