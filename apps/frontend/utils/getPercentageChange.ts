export const getPercentageChange = (a: number, b: number) => {
  return ((a - b) / a) * 100;
};
