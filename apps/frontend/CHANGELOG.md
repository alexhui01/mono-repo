## @jarvis-network/exchange-frontend [1.0.1](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/compare/@jarvis-network/exchange-frontend@1.0.0...@jarvis-network/exchange-frontend@1.0.1) (2020-12-23)


### Bug Fixes

* **frontend:** :bug: Fixed inverted prices ([94d6d51](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/94d6d51911061d3daee2157c9b06fc309ff771f9))
* **frontend:** :monocle_face: Change arithmetic from `a.mul(1.div(b))` to `a.div(b)` in `useExchangeValues` ([fc45091](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/fc45091975ece8aaccfb1ba00044385cf8f26b2c))
* **frontend:** :wrench: Change default price feed endpoint url to pricefeed-dev.jarvis.exchange ([c9e2d99](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/c9e2d993a8f24b111004aee7598030a4ea0c0a06))

# 1.0.0 (2020-12-22)


### Features

* **root:** :fire: Add semantic-versioning ([d7e9b4d](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/d7e9b4df5842e8531b4f85d0c5c1e24cd01d3e81))
* **root:** :hammer: Change the registry ([e712521](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/e712521eed64bd80bb81adbf04b23a76e3e7f08d))
* **root:** :tada: Move the devDependencies to root package.json ([5d4067a](https://gitlab.com/jarvis-network/apps/exchange/mono-repo/commit/5d4067a8c8e61107fb8814d3dce6feab02d0682d))
